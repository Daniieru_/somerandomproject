#pragma once
#include "Entity.h"
class ConvexEntity :															//custom convex-shaped entity
	public Entity
{
public:																			
	b2Body *m_Body;																//body that represents entity in physics world
	//   UNDER CONSTRUCTION ! ;-)
	ConvexEntity();
	ConvexEntity(b2World &world, b2Vec2 *vertices, int count, b2Vec2 pos, b2BodyType type, float friction = defFriction,
		float restitution = defRestitution, float density = defDesity, float AngDumping = defAngDumping);
	~ConvexEntity();

	void LoadTextureFromFile(std::string imgDir);
	void UpdateGraphics();
	sf::ConvexShape GetShape();
	void SetColours(sf::Color color = sf::Color::Green, float thickness = -1.0f, bool fill = false);

private:
	sf::Texture *m_Texture;														//graphics stuff 
	sf::ConvexShape m_Shape;													//graphics shape (to be textured)
};

