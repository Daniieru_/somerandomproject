#pragma once

#include <vector>
#include "RectangleEntity.h"
#include "CircleEntity.h"
#include "ConvexEntity.h"

class EntitiesManager
{
public:
;

	EntitiesManager();
	EntitiesManager(b2World &world);
	~EntitiesManager();

	void AddEntity(RectangleEntity entity);							//add a rectnangle-shaped entity
	void AddEntity(CircleEntity entity);							//add a circle-shaped entity
	void AddEntity(ConvexEntity entity);							//add a convex-shaped entity

	void PxWorldStep(float ts = timeStep, int vi = velocityIterations, int pi = positionIterations);

	void UpdateGraphics();
	void DrawAll(sf::RenderWindow &window);
	// ******************    TO     DO    **********************

private:
	b2World *m_pWorld;
	std::vector<RectangleEntity> m_vRects;
	std::vector<CircleEntity> m_vCirlces;
	std::vector<ConvexEntity> m_vConvexs;
};

