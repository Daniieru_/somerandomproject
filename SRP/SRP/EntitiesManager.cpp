#include "EntitiesManager.h"



EntitiesManager::EntitiesManager()
{
}

EntitiesManager::EntitiesManager(b2World & world)
{
	this->m_pWorld = &world;
}



EntitiesManager::~EntitiesManager()
{
}

void EntitiesManager::AddEntity(RectangleEntity entity)
{
	m_vRects.push_back(entity);
}

void EntitiesManager::AddEntity(CircleEntity entity)
{
	m_vCirlces.push_back(entity);
}

void EntitiesManager::AddEntity(ConvexEntity entity)
{
	m_vConvexs.push_back(entity);
}

void EntitiesManager::PxWorldStep(float ts, int vi, int pi)
{
	m_pWorld->Step(ts, vi, pi);
}

void EntitiesManager::UpdateGraphics()
{
	int i;
	for (i = 0; i < m_vCirlces.size(); i++)					//update circle shapes
	{
		m_vCirlces[i].UpdateGraphics();
	}
	for (i = 0; i < m_vRects.size(); i++)					//update rectangle shapes
	{
		m_vRects[i].UpdateGraphics();
	}
	for (i = 0; i < m_vConvexs.size(); i++)					//update convex shapes
	{
		m_vConvexs[i].UpdateGraphics();
	}
}

void EntitiesManager::DrawAll(sf::RenderWindow & window)
{
	int i;
	for (i = 0; i < m_vCirlces.size(); i++)					//draw circle shapes
	{
		window.draw(m_vCirlces[i].GetShape());
	}
	for (i = 0; i < m_vRects.size(); i++)					//draw rectangle shapes
	{
		window.draw(m_vRects[i].GetShape());
	}
	for (i = 0; i < m_vConvexs.size(); i++)					//draw convex shapes
	{
		window.draw(m_vConvexs[i].GetShape());
	}
}

