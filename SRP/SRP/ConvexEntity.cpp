#include "ConvexEntity.h"



ConvexEntity::ConvexEntity()
{
}

ConvexEntity::ConvexEntity(b2World & world, b2Vec2 * vertices, int count, b2Vec2 pos, b2BodyType type, float friction, float restitution, float density, float AngDumping)
{
	b2PolygonShape bShape;
	bShape.Set(vertices, count);

	b2BodyDef bDef;
	bDef.type = type; bDef.position.Set(pos.x*MPP, pos.y*MPP);

	m_Body = world.CreateBody(&bDef);



	b2FixtureDef bFix;
	bFix.shape = &bShape; bFix.friction = friction; bFix.restitution = restitution; bFix.density = density;

	m_Body->CreateFixture(&bFix);

	//sf::VertexArray vArray(sf::Lines, bShape.GetVertexCount());				//*** TO DO				

	int ptCount = bShape.GetVertexCount();

	m_Shape.setPointCount(ptCount);

	int currPt = 0;

	for (int i = ptCount - 1; i >= 0; i--)
	{
		/*vArray[i].position = sf::Vector2f(bShape.GetVertex(i).x, bShape.GetVertex(i).y);
		vArray[i].color = sf::Color::Green;*/

		m_Shape.setPoint(currPt, sf::Vector2f(bShape.GetVertex(i).x, bShape.GetVertex(i).y));
		currPt++;
	}
	//	m_Shape.setFillColor = sf::Color::Transparent; m_Shape.setOutlineColor = sf::Color::Green;
	//m_Shape.setOutlineThickness = 1.0f;

	UpdateGraphics();
}


ConvexEntity::~ConvexEntity()
{
}

void ConvexEntity::LoadTextureFromFile(std::string imgDir)
{
	if (!m_Texture->loadFromFile(imgDir))
		m_Texture->loadFromFile(ErrorImgDir);
	m_Shape.setTexture(m_Texture);
}

void ConvexEntity::UpdateGraphics()
{
	sf::Vector2f pos(m_Body->GetPosition().x * PPM, m_Body->GetPosition().y * PPM);
	m_Shape.setPosition(pos);
	m_Shape.setRotation((m_Body->GetAngle() * 180) / b2_pi);
}

sf::ConvexShape ConvexEntity::GetShape()
{
	return m_Shape;
}

void ConvexEntity::SetColours(sf::Color color, float thickness, bool fill)
{
	m_Shape.setOutlineColor(color);
	m_Shape.setOutlineThickness(thickness);
	(fill) ? m_Shape.setFillColor(color) : m_Shape.setFillColor(sf::Color::Transparent);
}
