#pragma once
#include <SFML\Graphics.hpp>
#include <Box2D\Box2D.h>
#include <Settings.h>
#include <string>

class Entity																	//Base class for entities			
{
public:
	b2Body *m_Body;																//body that represents entity in physics world

	Entity();
																				//Constructor for a circle-shaped entity
	Entity(b2World &world, float radius, b2Vec2 pos, b2BodyType type, float friction = defFriction, 
		float restitution = defRestitution, float density = defDesity, float AngDumping = defAngDumping);	

																				//Constructor for a rectangle-shaped entity
	Entity(b2World &world, float halfSizeW, float halfSizeH, b2Vec2 pos, b2BodyType type, float friction = defFriction,
		float restitution = defRestitution, float density = defDesity, float AngDumping = defAngDumping);

																				//Constructor for a custom polygon-shaped entity
	Entity(b2World &world, b2Vec2 *vertices, int count, b2Vec2 pos, b2BodyType type, float friction = defFriction,
		float restitution = defRestitution, float density = defDesity, float AngDumping = defAngDumping);


	~Entity();

	virtual void LoadTextureFromFile(std::string imgDir);
	virtual void UpdateGraphics() = NULL;
	virtual void SetColours(sf::Color color, float thickness, bool fill) = NULL;

private:
	sf::Texture *m_Texture;														//graphics stuff 
	sf::RectangleShape m_Shape;													//graphics shape (to be textured)
	//consider to change m_Shape to public
	sf::ConvexShape m_ConvexShape;
};

