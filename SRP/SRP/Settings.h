#pragma once

namespace settings
{
#define PPM 15.0f
#define MPP 1/PPM

#define defFriction 0.3f
#define defRestitution 0.2f
#define defDesity 0.7f
#define defAngDumping 0.7f
#define ErrorImgDir "ErrorImg.png"

#define timeStep 1.0f / 60.0f				// time step 
#define velocityIterations 8				// iterations:	more = accuracy
#define positionIterations 4				// 				less = performance
}