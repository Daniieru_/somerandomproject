#pragma once
#include "Entity.h"
class CircleEntity :															//circle shaped entity
	public Entity
{
public:
	b2Body *m_Body;																//body that represents entity in physics world

	CircleEntity();
																				//circle shaped entity
	CircleEntity(b2World &world, float radius, b2Vec2 pos, b2BodyType type, float friction = defFriction,
		float restitution = defRestitution, float density = defDesity, float AngDumping = defAngDumping);
	~CircleEntity();

	void LoadTextureFromFile(std::string imgDir);
	void UpdateGraphics();
	sf::CircleShape GetShape();
	void SetColours(sf::Color color = sf::Color::Green, float thickness = -1.0f, bool fill = false);

private:
	sf::Texture *m_Texture;														//graphics stuff 
	sf::CircleShape m_Shape;													//graphics shape (to be textured)
};	

