#include "RectangleEntity.h"



RectangleEntity::RectangleEntity()
{
}

RectangleEntity::RectangleEntity(b2World & world, float halfSizeW, float halfSizeH, b2Vec2 pos, b2BodyType type, float friction, float restitution, float density, float AngDumping)
{
	
	b2BodyDef bDef;
	bDef.type = type; bDef.position.Set(pos.x * MPP, pos.y * MPP);

	m_Body = world.CreateBody(&bDef);

	b2PolygonShape bShape;
	bShape.SetAsBox(halfSizeW * MPP, halfSizeH * MPP);

	b2FixtureDef bFix;
	bFix.shape = &bShape; bFix.friction = friction; bFix.restitution = restitution; bFix.density = density;

	m_Body->CreateFixture(&bFix);
	m_Body->SetAngularDamping(AngDumping);

	m_Shape.setSize(sf::Vector2f(2 * halfSizeW, 2 * halfSizeH));
	m_Shape.setOrigin(sf::Vector2f(halfSizeW, halfSizeH));
	m_Shape.setFillColor(sf::Color::Transparent); m_Shape.setOutlineColor(sf::Color::Green); m_Shape.setOutlineThickness(-1.0f);

}


RectangleEntity::~RectangleEntity()
{
}

void RectangleEntity::LoadTextureFromFile(std::string imgDir)
{
	if (!m_Texture->loadFromFile(imgDir))
		m_Texture->loadFromFile(ErrorImgDir);
	m_Shape.setTexture(m_Texture);
}

void RectangleEntity::UpdateGraphics()
{
	sf::Vector2f pos(m_Body->GetPosition().x * PPM, m_Body->GetPosition().y * PPM);
	m_Shape.setPosition(pos);
	m_Shape.setRotation((m_Body->GetAngle() * 180) / b2_pi);
}

sf::RectangleShape RectangleEntity::GetShape()
{
	return m_Shape;
}

void RectangleEntity::SetColours(sf::Color color, float thickness, bool fill)
{
	m_Shape.setOutlineColor(color);
	m_Shape.setOutlineThickness(thickness);
	(fill) ? m_Shape.setFillColor(color) : m_Shape.setFillColor(sf::Color::Transparent);
}
